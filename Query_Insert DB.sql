create database refresh_db

use refresh_db

SET IDENTITY_INSERT tb_form_covid off
INSERT tb_form_covid(
			Id, 
			Email,
			Nama,
			NPM, 
			Umur,
			Jenis_Kelamin,
			No_Telp,
			Alamat_Sekarang,
			Alamat_Asal,
			Lokasi,
			Created_By,
			Created_On,
			Modified_By,
			Modified_On,
			Is_Deleted,
			Deleted_By,
			Deleted_On)
VALUES (1, 'damidamian@email.com', 'Dummy Mustopa', '15002913', '28', '1', '081231416283', 'Jakarta Selatan', 'Bukit Duri', 'Karawaci', 'Eiffel', '2020-7-20', null, null, 0, null, null)

SET IDENTITY_INSERT tb_gender On
INSERT tb_gender(
			Id, 
			Jenis_Kelamin)
VALUES (2, 'Perempuan')
