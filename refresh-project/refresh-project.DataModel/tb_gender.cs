namespace refresh_project.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tb_gender
    {
        public long Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Jenis_Kelamin { get; set; }
    }
}
