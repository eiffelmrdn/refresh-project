namespace refresh_project.DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class refresh_projectContext : DbContext
    {
        public refresh_projectContext()
            : base("name=refreshContext")
        {
        }

        public virtual DbSet<tb_form_covid> tb_form_covid { get; set; }
        public virtual DbSet<tb_gender> tb_gender { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.Nama)
                .IsUnicode(false);

            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.NPM)
                .IsUnicode(false);

            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.No_Telp)
                .IsUnicode(false);

            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.Alamat_Sekarang)
                .IsUnicode(false);

            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.Alamat_Asal)
                .IsUnicode(false);

            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.Lokasi)
                .IsUnicode(false);

            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.Modified_By)
                .IsUnicode(false);

            modelBuilder.Entity<tb_form_covid>()
                .Property(e => e.Deleted_By)
                .IsUnicode(false);

            modelBuilder.Entity<tb_gender>()
                .Property(e => e.Jenis_Kelamin)
                .IsUnicode(false);
        }
    }
}
