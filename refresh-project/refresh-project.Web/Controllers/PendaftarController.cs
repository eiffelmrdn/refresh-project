﻿using DocumentFormat.OpenXml.Office2010.Excel;
using refresh_project.DataModel;
using refresh_project.Repository;
using refresh_project.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace refresh_project.Web.Controllers
{
    public class PendaftarController : Controller
    {
        // GET: Pendaftar
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            ViewBag.jeniskelamin = PendaftarRepo.genderList();
            List<tb_form_covid> list = PendaftarRepo.GetAll();
            return PartialView("_List", list);
        }

        public ActionResult Details(long id)
        {
            ViewBag.jeniskelamin = PendaftarRepo.genderName(id);
            return PartialView("_Details", PendaftarRepo.ById(id));
        }

        public ActionResult Edit(long id)
        {
            return PartialView("_Edit", PendaftarRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Edit(PendaftarViewModel model)
        {
            try
            {
                ResponseResult result = new ResponseResult();
                PendaftarRepo.alert = string.Empty;
                
                //VALIDASI EMAIL
                String[] val_email = model.Email.Split('@');
                if (val_email.Length == 1)
                {
                    PendaftarRepo.alert = "Format email salah. ";
                }

                //VALIDASI NO.HP
                char[] hp = model.No_Telp.ToCharArray();                
                for (int i = 0; i < 2; i++)
                {
                    if (hp[0] != '0' && hp[1] != '8')
                    {
                        PendaftarRepo.alert = "Format email salah. ";
                        break;
                    }
                }

                using (var db = new refresh_projectContext())
                {
                    var data = db.tb_form_covid.ToList();
                    //VALIDASI EMAIL DENGAN DATABASE
                    if (data.Any(o => o.Email == model.Email && o.Id != model.Id))
                    {
                        PendaftarRepo.alert = "Email sudah pernah digunakan.";
                    }
                    //VALIDASI NO. HP DENGAN DATABASE
                    if (data.Any(o => o.No_Telp == model.No_Telp && o.Id != model.Id))
                    {
                        PendaftarRepo.alert = "No. HP sudah pernah digunakan.";
                    }
                    //VALIDASI IDENTITAS DENGAN DATABASE
                    if (data.Any(o => o.NPM == model.NPM && o.Id != model.Id))
                    {
                        PendaftarRepo.alert = "NPM/NIS sudah pernah digunakan.";
                    }
                }

                if (string.IsNullOrEmpty(PendaftarRepo.alert))
                {
                    return Json(
                    new
                    {
                        success = PendaftarRepo.Update(model),
                        message = result.Message,
                    },
                    JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(
                    new
                    {
                        success = false,
                        message = PendaftarRepo.alert,
                    },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        success = false,
                        message = ex.Message,
                    },
                    JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Create()
        {
            return PartialView("_Create");
        }

    }
}
