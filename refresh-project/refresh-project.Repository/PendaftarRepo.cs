﻿using refresh_project.DataModel;
using refresh_project.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace refresh_project.Repository
{
    public class PendaftarRepo
    {
        public static string alert;

        public static string genderName(long id)
        {
            int angka = Convert.ToInt32(id);
            string namaGender = "";
            List<tb_form_covid> user = new List<tb_form_covid>();
            List<tb_gender> jk = new List<tb_gender>();
            using (var db = new refresh_projectContext())
            {
                user = db.tb_form_covid.ToList();
                jk = db.tb_gender.ToList();
                for (int i = 0; i < angka; i++)
                {
                    for (int j = 0; j < jk.Count; j++)
                    {
                        if (user[i].Jenis_Kelamin == jk[j].Id)
                        {
                            namaGender = jk[j].Jenis_Kelamin;
                        }
                    }
                }
            }
            return namaGender;
        }

        public static string genderList()
        {
            //int angka = Convert.ToInt32(id);
            string namaGender = "";
            List<tb_form_covid> user = new List<tb_form_covid>();
            List<tb_gender> jk = new List<tb_gender>();
            using (var db = new refresh_projectContext())
            {
                user = db.tb_form_covid.ToList();
                jk = db.tb_gender.ToList();
                for (int i = 0; i < user.Count; i++)
                {
                    for (int j = 0; j < jk.Count; j++)
                    {
                        if (user[i].Jenis_Kelamin == jk[j].Id)
                        {
                            namaGender = jk[j].Jenis_Kelamin;
                        }
                    }
                }
            }
            return namaGender;
        }

        public static List<tb_form_covid> GetAll()
        {
            List<tb_form_covid> result = new List<tb_form_covid>();
            using (var db = new refresh_projectContext())
            {
                result = db.tb_form_covid
                    .Where(o => o.Is_Deleted == false)
                    .ToList();
            }
            return result;
        }

        public static PendaftarViewModel ById(long id)
        {
            PendaftarViewModel result = new PendaftarViewModel();
            try
            {
                using (var db = new refresh_projectContext())
                {
                    result = (from a in db.tb_form_covid
                              where a.Id == id
                              select new PendaftarViewModel
                              {
                                  Nama = a.Nama,
                                  Email = a.Email,
                                  No_Telp = a.No_Telp,  
                                  Umur = a.Umur,                                
                                  Alamat_Sekarang = a.Alamat_Sekarang,
                                  Alamat_Asal = a.Alamat_Asal,
                                  Jenis_Kelamin = a.Jenis_Kelamin,
                                  NPM = a. NPM,
                                  Lokasi = a.Lokasi,
                              }).FirstOrDefault();
                }
                if (result == null)
                {
                    result = new PendaftarViewModel();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result == null ? new PendaftarViewModel() : result;
        }

        public static ResponseResult Update(PendaftarViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {                
                using (var db = new refresh_projectContext())
                {
                    tb_form_covid pendaftar = db.tb_form_covid.Where(o => o.Id == entity.Id).FirstOrDefault();
                    if (entity.Id == 0)
                    {
                        //CREATE
                        tb_form_covid data = new tb_form_covid();
                        data.Nama = entity.Nama;
                        data.Email = entity.Email;
                        data.No_Telp = entity.No_Telp;
                        data.NPM = entity.NPM;
                        data.Umur = entity.Umur;
                        data.Jenis_Kelamin = entity.Jenis_Kelamin;
                        data.Lokasi = entity.Lokasi;
                        data.Alamat_Asal = entity.Alamat_Asal;
                        data.Alamat_Sekarang = entity.Alamat_Sekarang;
                        data.Created_By = "Eiffel";
                        data.Created_On = DateTime.Now;
                        data.Is_Deleted = false;
                        db.tb_form_covid.Add(data);
                        db.SaveChanges();
                        result.Message = "Pendaftaran Berhasil!";
                        result.Entity = entity;
                    }
                    else
                    {                       
                        if (pendaftar == null)
                        {
                            result.Success = false;
                        }
                        else
                        {
                            //EDIT
                            pendaftar.Nama = entity.Nama;
                            pendaftar.Email = entity.Email;
                            pendaftar.No_Telp = entity.No_Telp;
                            pendaftar.NPM = entity.NPM;
                            pendaftar.Umur = entity.Umur;
                            pendaftar.Jenis_Kelamin = entity.Jenis_Kelamin;
                            pendaftar.Lokasi = entity.Lokasi;
                            pendaftar.Alamat_Asal = entity.Alamat_Asal;
                            pendaftar.Alamat_Sekarang = entity.Alamat_Sekarang;
                            pendaftar.Modified_By = "Eiffel";
                            pendaftar.Modified_On = DateTime.Now;
                            pendaftar.Is_Deleted = false;
                            pendaftar.Modified_By = "Admin";
                            pendaftar.Modified_On = DateTime.Now;
                            db.SaveChanges();
                            result.Message = "Data Berhasil Diubah!";
                            result.Entity = entity;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        //public static ResponseResult Update(BiodataViewModel entity)
        //{
        //    ResponseResult result = new ResponseResult();
        //    try
        //    {
        //        using (var db = new refresh_projectContext())
        //        {
        //            x_biodata Biodata = db.x_biodata.Where(o => o.id == entity.id).FirstOrDefault();
        //            x_address Address = db.x_address.Where(o => o.biodata_id == entity.id).FirstOrDefault();
        //            var data = db.x_biodata.ToList();

        //            if (Biodata != null)
        //            {
        //                //JIKA SAMA KAYAK ID LAIN MAKA DI-SKIP
        //                //EMAIL
        //                if (data.Any(o => o.email != entity.email && o.id != entity.id))
        //                {
        //                    //CEK FORMAT
        //                    String[] val_email = entity.email.Split('@');
        //                    if (val_email.Length != 1)
        //                    {
        //                        Biodata.email = entity.email;
        //                    }
        //                }
        //                //IDENTITAS
        //                //CEK JIKA BEDA DENGAN ID LAIN
        //                if (data.Any(o => o.identity_type_id == entity.identity_type_id && o.identity_no != entity.identity_no && o.id != entity.id))
        //                {
        //                    //CEK DENGAN DATA LAMANYA SENDIRI
        //                    if (data.Any(o => o.identity_no != entity.identity_no || o.identity_type_id != entity.identity_type_id && o.id == entity.id))
        //                    {
        //                        Biodata.identity_no = entity.identity_no;
        //                        Biodata.identity_type_id = entity.identity_type_id;
        //                    }
        //                }
        //                else
        //                if (data.Any(o => o.identity_type_id != entity.identity_type_id && o.identity_no == entity.identity_no && o.id != entity.id))
        //                {
        //                    //CEK DENGAN DATA LAMANYA SENDIRI
        //                    if (data.Any(o => o.identity_no != entity.identity_no || o.identity_type_id != entity.identity_type_id && o.id == entity.id))
        //                    {
        //                        Biodata.identity_no = entity.identity_no;
        //                        Biodata.identity_type_id = entity.identity_type_id;
        //                    }
        //                }

        //                //JIKA STATUS NIKAH LAJANG
        //                if (entity.marital_status_id == 1)
        //                {
        //                    Biodata.marriage_year = null;
        //                    Biodata.marital_status_id = entity.marital_status_id;
        //                }
        //                else
        //                {
        //                    Biodata.marital_status_id = entity.marital_status_id;
        //                    Biodata.marriage_year = entity.marriage_year;
        //                }

        //                //FORMAT
        //                //NO. HANDPHONE
        //                char[] hp = entity.phone_number1.ToCharArray();
        //                char[] php = entity.parent_phone_number.ToCharArray();
        //                if (entity.phone_number2 != null) //Jika ada
        //                {
        //                    char[] ahp = entity.phone_number2.ToCharArray();
        //                    if (ahp[0] == '0' && ahp[1] == '8')
        //                    {
        //                        Biodata.phone_number2 = entity.phone_number2;
        //                    }
        //                }
        //                if (hp[0] == '0' && hp[1] == '8' &&
        //                        php[0] == '0' && php[1] == '8')
        //                {
        //                    if (data.Any(o => o.phone_number1 != entity.phone_number1 && o.id != entity.id))
        //                    {
        //                        Biodata.phone_number1 = entity.phone_number1;
        //                    }
        //                    Biodata.parent_phone_number = entity.parent_phone_number;
        //                }


        //                //JUMLAH SAUDARA
        //                int anakKe = Convert.ToInt32(entity.child_sequence);
        //                int totalsaudara = Convert.ToInt32(entity.how_many_brothers);
        //                //Validasi Inputan angka
        //                string[] anak = new string[] { entity.child_sequence };
        //                string[] sodara = new string[] { entity.how_many_brothers };
        //                if (anak.Length < 2 && sodara.Length < 2)
        //                {
        //                    //Validasi jumlah
        //                    if (anakKe <= totalsaudara)
        //                    {
        //                        Biodata.child_sequence = entity.child_sequence;
        //                        Biodata.how_many_brothers = entity.how_many_brothers;
        //                    }
        //                }

        //                Biodata.fullname = entity.fullname;
        //                Biodata.nick_name = entity.nick_name;
        //                Biodata.pob = entity.pob;
        //                Biodata.dob = entity.dob;
        //                Biodata.gender = entity.gender;
        //                Biodata.religion_id = entity.religion_id;
        //                Biodata.high = entity.high;
        //                Biodata.weight = entity.weight;
        //                Biodata.nationality = entity.nationality;
        //                Biodata.ethnic = entity.ethnic;
        //                Biodata.hobby = entity.hobby;
        //                Biodata.email = entity.email;

        //                Biodata.modified_by = 797;
        //                Biodata.modified_on = DateTime.Now;

        //                Address.address1 = entity.address1;
        //                Address.postal_code1 = entity.postal_code1;
        //                Address.rt1 = entity.rt1;
        //                Address.rw1 = entity.rw1;
        //                Address.kelurahan1 = entity.kelurahan1;
        //                Address.kecamatan1 = entity.kecamatan1;
        //                Address.region1 = entity.region1;
        //                Address.address2 = entity.address2;
        //                Address.postal_code2 = entity.postal_code2;
        //                Address.rt2 = entity.rt2;
        //                Address.rw2 = entity.rw2;
        //                Address.kelurahan2 = entity.kelurahan2;
        //                Address.kecamatan2 = entity.kecamatan2;
        //                Address.region2 = entity.region2;
        //                Address.modified_by = 797;
        //                Address.modified_on = DateTime.Now;

        //                db.SaveChanges();
        //                result.Message = "Updated";
        //                result.Entity = entity;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Message = ex.Message;
        //        result.Success = false;
        //    }
        //    return result;
        //}
    }
}
