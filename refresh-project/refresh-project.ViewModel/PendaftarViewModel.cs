﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace refresh_project.ViewModel
{
    public class PendaftarViewModel
    {
        public long Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        [StringLength(50)]
        public string Nama { get; set; }

        [Required]
        [StringLength(8)]
        public string NPM { get; set; }

        public long Umur { get; set; }

        public long Jenis_Kelamin { get; set; }

        [Required]
        [StringLength(13)]
        public string No_Telp { get; set; }

        [Required]
        [StringLength(100)]
        public string Alamat_Sekarang { get; set; }

        [Required]
        [StringLength(100)]
        public string Alamat_Asal { get; set; }

        [StringLength(20)]
        public string Lokasi { get; set; }
        [StringLength(10)]
        public string Created_By { get; set; }

        public DateTime? Created_On { get; set; }

        [StringLength(10)]
        public string Modified_By { get; set; }

        public DateTime? Modified_On { get; set; }

        public bool Is_Deleted { get; set; }

        [StringLength(10)]
        public string Deleted_By { get; set; }

        public DateTime? Deleted_On { get; set; }
    }
}
